//http://www.java2s.com/Code/Java/Collections-Data-Structure/LinearInterpolation.htm
package Maths;

import java.util.Arrays;

public class interpLinear {
    //x & y are given; xi represents the values we want to perform the interpolation
    public static final double[] interpLinear(double[] x, double[] y, double[] xi, boolean linearExtrapolation) throws IllegalArgumentException {

        if (x.length != y.length) {
            throw new IllegalArgumentException("X and Y must be the same length");
        }
        if (x.length == 1) {
            throw new IllegalArgumentException("X must contain more than one value");
        }
        double[] dx = new double[x.length - 1];
        double[] dy = new double[x.length - 1];
        double[] slope = new double[x.length - 1];
        double[] intercept = new double[x.length - 1];

        // Calculate the line equation (i.e. slope and intercept) between each point
        for (int i = 0; i < x.length - 1; i++) {
            dx[i] = x[i + 1] - x[i];
            if (dx[i] == 0) {
                throw new IllegalArgumentException("X must be montotonic. A duplicate " + "x-value was found");
            }
            if (dx[i] < 0) {
                throw new IllegalArgumentException("X must be sorted");
            }
            dy[i] = y[i + 1] - y[i];
            slope[i] = dy[i] / dx[i];
            intercept[i] = y[i] - x[i] * slope[i];
        }

        // Perform the interpolation here
        double[] yi = new double[xi.length];
        for (int i = 0; i < xi.length; i++) {
            if (xi[i] > x[x.length - 1]) {
                if (linearExtrapolation == true) {
                    double deltay = y[x.length - 1] - y[x.length - 2];
                    double deltax = x[x.length - 1] - x[x.length - 2];
                    yi[i] = y[x.length - 1] + (xi[i] - x[x.length - 1]) * (deltay / deltax);
                    if (yi[i] < 0.) {
                        yi[i] = 0.;
                    }
                } else {
                    yi[i] = y[x.length - 1];
                }
            } else if (xi[i] < x[0]) {
                if (linearExtrapolation == true) {
                    double deltay = y[1] - y[0];
                    double deltax = x[1] - x[0];
                    yi[i] = y[0] + (xi[i] - x[0]) * (deltay / deltax);
                    if (yi[i] < 0.) {
                        yi[i] = 0.;
                    }
                } else {
                    yi[i] = y[0];
                }
            } else {
                int loc = Arrays.binarySearch(x, xi[i]);
                if (loc < -1) {
                    loc = -loc - 2;
                    yi[i] = slope[loc] * xi[i] + intercept[loc];
                } else {
                    yi[i] = y[loc];
                }
            }
        }

        return yi;
    }
}
