package DataManipulation;

import GlobalVariables.Globals;
import Pricing.BlackScholesFormula;

import javax.swing.table.TableModel;

public class Checking {

    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------

    public static void checkVolsReconcileWithPrices(TableModel t, boolean price) {
        int i = 0;
        int cDt = 0;
        int cTm = 1;
        int cF = 2;
        int cK = 3;
        int cV = 4;
        int cP = 5;
        int dataRows = t.getRowCount();

        String dt;
        double tm, F, K, v, pBS, p, maxDiff;

        while (i < dataRows) {
            dt = t.getValueAt(i, cDt).toString();
            tm = Double.parseDouble(t.getValueAt(i, cTm).toString());
            F = Double.parseDouble(t.getValueAt(i, cF).toString());
            K = Double.parseDouble(t.getValueAt(i, cK).toString());
            v = Double.parseDouble(t.getValueAt(i, cV).toString());
            p = Double.parseDouble(t.getValueAt(i, cP).toString());
            v = v / 100;

            if (price == true) {
                pBS = BlackScholesFormula.calculate(true, F, K, 0.0, tm, v);
            } else {
                //rates instead of price
                throw new Error("");
            }

            maxDiff = pBS * Globals.toleranceAsFraction;

            if (Math.abs(pBS - p) > maxDiff) {
                String message = "checkVolsReconcileWithPrices : disagreement between calculated price and quoted price.";
                message += "Row = " + i + "\n";
                message += "Date = " + dt + "\n";
                message += "Forward = " + F + "\n";
                message += "K = " + K + "\n";
                message += "v = " + v + "\n";
                message += "p = " + p + "\n";
                message += "theoretical price = " + pBS + "\n";
                throw new Error(message);
            }

            i++;
        }
    }

    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------

    public static class ReinterpolationMetrics {
        public boolean performReinterpolation;
        public double diff;
        public double absdiff;
        public double max_diff;
        public double max_absdiff;
        public double tot_diff;
        public double tot_absdiff;
        public int count;

        public ReinterpolationMetrics() {
            performReinterpolation = false;
            diff = 0.;
            absdiff = 0.;
            max_diff = 0.;
            max_absdiff = 0.;
            tot_diff = 0.;
            tot_absdiff = 0.;
        }

        void printMetrics() {
            if (performReinterpolation == true) {
                System.out.println("ReinterpolationMetrics : max_diff = " + max_diff);
                System.out.println("ReinterpolationMetrics : max_absdiff = " + max_absdiff);
                System.out.println("ReinterpolationMetrics : ave_diff = " + (tot_diff / ((double) count)));
                System.out.println("ReinterpolationMetrics : ave_absdiff = " + (tot_absdiff / ((double) count)));
            } else {
                System.out.println("ReinterpolationMetrics : performReinterpolation is false");
            }
        }
    }

    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------

}
