package DataManipulation;

import Maths.interpLinear;
import Pricing.BlackScholesFormula;
import com.gs.collections.api.list.MutableList;
import com.gs.collections.impl.list.mutable.FastList;

import java.io.*;

import javax.swing.table.TableModel;

import Parsing.CSVParser;

public class Interpolation {

    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------

    //if "useLinearInVol" is false, then the variance is linearly interpolated
    public static Data.VolsForGivenDt performStrikeInterpolation(Data.VolsForGivenDt t, double[] strikeGrid,
                                                                 Checking.ReinterpolationMetrics check,
                                                                 boolean useLinearInVol) {
        Data.VolsForGivenDt volsDtInterp = new Data.VolsForGivenDt();
        boolean linearExtrapolation = true;
        volsDtInterp.dt = t.dt;
        for (int indexFut = 0; indexFut < t.volsForDt.size(); indexFut++) {
            Data.VolsForGivenFut volsFut = t.volsForDt.get(indexFut);
            Data.VolsForGivenFut volsFutInterp = new Data.VolsForGivenFut();
            volsFutInterp.F = volsFut.F;
            for (int indexTm = 0; indexTm < volsFut.volsForFut.size(); indexTm++) {
                Data.VolsForGivenTm volsTm = volsFut.volsForFut.get(indexTm);
                double x[] = volsTm.ln_K_over_S;
                double y[] = volsTm.v;
                double[] yGridVals;
                if (x.length == 1) {
                    yGridVals = new double[strikeGrid.length];
                    for (int i = 0; i < strikeGrid.length; i++) {
                        yGridVals[i] = y[0];
                    }
                } else {
                    if (useLinearInVol == true) {
                        yGridVals = interpLinear.interpLinear(x, y, strikeGrid, linearExtrapolation);
                    } else {
                        double[] vars = new double[y.length];
                        for (int i = 0; i < vars.length; i++) {
                            vars[i] = Math.pow(y[i], 2.0);
                        }
                        yGridVals = interpLinear.interpLinear(x, vars, strikeGrid, linearExtrapolation);
                        for (int i = 0; i < yGridVals.length; i++) {
                            yGridVals[i] = Math.pow(yGridVals[i], 0.5);
                        }
                    }
                }
                if (check.performReinterpolation == true) {
                    double[] yReinterp;
                    if (strikeGrid.length == 1) {
                        yReinterp = new double[x.length];
                        for (int i = 0; i < x.length; i++) {
                            yReinterp[i] = yGridVals[0];
                        }
                    } else {
                        yReinterp = interpLinear.interpLinear(strikeGrid, yGridVals, x, linearExtrapolation);
                    }
                    for (int i = 0; i < yReinterp.length; i++) {
                        check.diff = yReinterp[i] - y[i];
                        check.absdiff = Math.abs(check.diff);
                        check.max_diff = Math.max(check.max_diff, check.diff);
                        check.max_absdiff = Math.max(check.max_absdiff, check.absdiff);
                        check.tot_diff += check.diff;
                        check.tot_absdiff += check.absdiff;
                        check.count += 1;
                    }
                }

                Data.VolsForGivenTm volsTmInterp = new Data.VolsForGivenTm(strikeGrid.length);
                volsTmInterp.ln_K_over_S = strikeGrid;
                volsTmInterp.v = yGridVals;
                volsTmInterp.tm = volsTm.tm;
                volsFutInterp.volsForFut.add(volsTmInterp);
            }
            volsDtInterp.volsForDt.add(volsFutInterp);
        }
        return volsDtInterp;
    }

    //if "useLinearInVolForTm" is false, then the variance*T is linearly interpolated
    public static Data.VolsForGivenDt performTmInterpolation(Data.VolsForGivenDt t, double[] tmGrid, boolean useLinearInVolForTm) {
        Data.VolsForGivenDt volsTimeInterp = new Data.VolsForGivenDt();
        volsTimeInterp.dt = t.dt;
        volsTimeInterp.volsForDt = FastList.newList();
        for (int indexFut = 0; indexFut < t.volsForDt.size(); indexFut++) {
            Data.VolsForGivenFut FutInterp = new Data.VolsForGivenFut();
            Data.VolsForGivenFut volsForFut = t.volsForDt.get(indexFut);
            FutInterp.F = volsForFut.F;
            FutInterp.volsForFut = FastList.newList();
            for (int indexTm = 1; indexTm < volsForFut.volsForFut.size(); indexTm++) {
                if (volsForFut.volsForFut.get(indexTm).ln_K_over_S.length != volsForFut.volsForFut.get(indexTm - 1).ln_K_over_S.length) {
                    throw new Error("performTmInterpolation : number of strikes varies across the time slices");
                }
            }
            for (int i = 0; i < tmGrid.length; i++) {
                Data.VolsForGivenTm volsTime = new Data.VolsForGivenTm(volsForFut.volsForFut.get(0).ln_K_over_S.length);
                volsTime.tm = tmGrid[i];
                volsTime.ln_K_over_S = volsForFut.volsForFut.get(0).ln_K_over_S;
                FutInterp.volsForFut.add(volsTime);
            }
            int numInputTimeSlices = volsForFut.volsForFut.size();
            if (numInputTimeSlices > 1) {
                double[] tm = new double[numInputTimeSlices];
                double[] volsInput = new double[numInputTimeSlices];
                double[] volsOutput;
                for (int indexTm = 0; indexTm < numInputTimeSlices; indexTm++) {
                    tm[indexTm] = volsForFut.volsForFut.get(indexTm).tm;
                }
                for (int indexStrike = 0; indexStrike < volsForFut.volsForFut.get(0).ln_K_over_S.length; indexStrike++) {
                    for (int indexTm = 0; indexTm < numInputTimeSlices; indexTm++) {
                        volsInput[indexTm] = volsForFut.volsForFut.get(indexTm).v[indexStrike];
                    }
                    //Note that if we used flat extrapolation, when we would need to correct
                    //for the fact that at the edges the times are anchored to the times
                    //of the edges.
                    if (useLinearInVolForTm == true) {
                        boolean linearExtrapolation = false;
                        volsOutput = interpLinear.interpLinear(tm, volsInput, tmGrid, linearExtrapolation);
                    } else {
                        volsOutput = interpolateAsVariance(tm, volsInput, tmGrid);
                    }
                    for (int i = 0; i < tmGrid.length; i++) {
                        FutInterp.volsForFut.get(i).v[indexStrike] = volsOutput[i];
                    }
                }
            } else {
                for (int i = 0; i < tmGrid.length; i++) {
                    FutInterp.volsForFut.get(i).v = volsForFut.volsForFut.get(0).v;
                }
            }
            volsTimeInterp.volsForDt.add(FutInterp);
        }
        return volsTimeInterp;
    }

    //This linearly interpolates on the variance*(time to maturity)
    //vols are the input vols
    //tm are the input times
    //tmGrid is the target time slices. The variances are outputted for these time slices
    public static double[] interpolateAsVariance(double[] tm, double[] vols, double[] tmGrid) {
        boolean linearExtrapolation = false;
        if (vols.length != tm.length) {
            throw new Error("interpolateAsVariance : vols and tm must be of same length");
        }
        if (tm.length == 1) {
            double[] returnVols = new double[tmGrid.length];
            for (int i = 0; i < tmGrid.length; i++) {
                returnVols[i] = vols[0];
            }
            return returnVols;
        } else {
            double[] vars = new double[tm.length];
            for (int i = 0; i < tm.length; i++) {
                vars[i] = Math.pow(vols[i], 2.0) * tm[i];
            }
            double[] varsInterp = interpLinear.interpLinear(tm, vars, tmGrid, linearExtrapolation);
            //Note that because flat extrapolation is used, the T in variance*T is
            //anchored to the extrapolation points, and below we correct for that.
            for (int i = 0; i < varsInterp.length; i++) {
                if (tmGrid[i] < tm[0]) {
                    varsInterp[i] = Math.sqrt(varsInterp[i] / tm[0]);
                } else if (tmGrid[i] > tm[tm.length - 1]) {
                    varsInterp[i] = Math.sqrt(varsInterp[i] / tm[tm.length - 1]);
                } else {
                    varsInterp[i] = Math.sqrt(varsInterp[i] / tmGrid[i]);
                }
            }
            return varsInterp;
        }
    }

    //Here "originalData" is the original market data and "interpolatedData"
    //is the result of performing interpolation onto the strikes of the vol
    //grid (but the times to maturities at this stage are still the same
    //as the original input market data). The following constraints have
    //to be satisfied.
    //a)The dates have to be the same,
    //b)The number of futures for each date have to be the same
    //c)For each future, the future values must be the same
    //d)For each future, the number of times to maturity must be the same and also
    //the values of the times to maturities must be the same
    //Note however for a given time slice, the number of strikes can be different.
    //if "useLinearInVol" is false, then the variance is linearly interpolated
    public static Data.VolsForGivenDt performStrikeReinterpolated(Data.VolsForGivenDt originalData,
                                                                  Data.VolsForGivenDt interpolatedData,
                                                                  boolean useLinearInVol) {
        Data.VolsForGivenDt volsDtInterp = new Data.VolsForGivenDt();
        boolean linearExtrapolation = true;
        volsDtInterp.dt = originalData.dt;

        if (interpolatedData.dt != originalData.dt) {
            throw new Error("performStrikeReinterpolated : mismatch between original and interpolated data");
        }
        if (interpolatedData.volsForDt.size() != originalData.volsForDt.size()) {
            throw new Error("performStrikeReinterpolated : mismatch between original and interpolated data");
        }

        for (int indexFut = 0; indexFut < originalData.volsForDt.size(); indexFut++) {
            Data.VolsForGivenFut volsFut = originalData.volsForDt.get(indexFut);

            if (interpolatedData.volsForDt.get(indexFut).F != volsFut.F) {
                throw new Error("performStrikeReinterpolated : mismatch between original and interpolated data");
            }
            if (interpolatedData.volsForDt.get(indexFut).volsForFut.size() != volsFut.volsForFut.size()) {
                throw new Error("performStrikeReinterpolated : mismatch between original and interpolated data");
            }

            Data.VolsForGivenFut volsFutInterp = new Data.VolsForGivenFut();
            volsFutInterp.F = volsFut.F;

            for (int indexTm = 0; indexTm < volsFut.volsForFut.size(); indexTm++) {
                Data.VolsForGivenTm volsTm = volsFut.volsForFut.get(indexTm);

                if (interpolatedData.volsForDt.get(indexFut).volsForFut.get(indexTm).tm != volsTm.tm) {
                    throw new Error("performStrikeReinterpolated : mismatch between original and interpolated data");
                }

                double strikeGrid[] = volsTm.ln_K_over_S;
                double[] y = interpolatedData.volsForDt.get(indexFut).volsForFut.get(indexTm).v;
                double[] x = interpolatedData.volsForDt.get(indexFut).volsForFut.get(indexTm).ln_K_over_S;

                double[] yGridVals;

                if (x.length == 1) {
                    yGridVals = new double[strikeGrid.length];
                    for (int i = 0; i < yGridVals.length; i++) {
                        yGridVals[i] = y[0];
                    }
                } else {
                    if (useLinearInVol == true) {
                        yGridVals = interpLinear.interpLinear(x, y, strikeGrid, linearExtrapolation);
                    } else {
                        double[] vars = new double[y.length];
                        for (int i = 0; i < vars.length; i++) {
                            vars[i] = Math.pow(y[i], 2.0);
                        }
                        yGridVals = interpLinear.interpLinear(x, vars, strikeGrid, linearExtrapolation);
                        for (int i = 0; i < yGridVals.length; i++) {
                            yGridVals[i] = Math.pow(yGridVals[i], 0.5);
                        }
                    }
                }

                Data.VolsForGivenTm volsTmInterp = new Data.VolsForGivenTm(strikeGrid.length);
                volsTmInterp.ln_K_over_S = strikeGrid;
                volsTmInterp.v = yGridVals;
                volsTmInterp.tm = volsTm.tm;
                volsFutInterp.volsForFut.add(volsTmInterp);
            }
            volsDtInterp.volsForDt.add(volsFutInterp);
        }
        return volsDtInterp;
    }

    //See comments for "performStrikeReinterpolated"
    //The following checks are implemented :
    //a)The dates have to be the same,
    //b)The number of futures for each date have to be the same
    //c)For each future, the future values must be the same
    //d)For each future,and each time to maturity, the number of strikes
    //and the strike values must be the same.
    //Note however that the number of time slices for a given future
    //can be different
    //if "useLinearInVolForTm" is false, then the variance*T is linearly interpolated
    public static Data.VolsForGivenDt performTmReinterpolated(Data.VolsForGivenDt originalData,
                                                              Data.VolsForGivenDt interpolatedData,
                                                              boolean useLinearInVolForTm) {
        Data.VolsForGivenDt volsDtInterp = new Data.VolsForGivenDt();
        volsDtInterp.dt = originalData.dt;
        if (interpolatedData.dt != originalData.dt) {
            throw new Error("performTmReinterpolated : mismatch between original and interpolated data");
        }
        //check that we have the same number of futures between interpolated and original data
        if (interpolatedData.volsForDt.size() != originalData.volsForDt.size()) {
            throw new Error("performTmReinterpolated : mismatch between original and interpolated data");
        }
        for (int indexFut = 0; indexFut < originalData.volsForDt.size(); indexFut++) {
            Data.VolsForGivenFut origVolsForFut = originalData.volsForDt.get(indexFut);
            //check that the actual future values are the same between interpolated and original data
            if (interpolatedData.volsForDt.get(indexFut).F != origVolsForFut.F) {
                throw new Error("performTmReinterpolated : mismatch between original and interpolated data");
            }
            Data.VolsForGivenFut volsFutInterp = new Data.VolsForGivenFut();
            volsFutInterp.F = origVolsForFut.F;
            double[] tmGridOrig = new double[originalData.volsForDt.get(indexFut).volsForFut.size()];
            for (int i = 0; i < tmGridOrig.length; i++) {
                tmGridOrig[i] = originalData.volsForDt.get(indexFut).volsForFut.get(i).tm;
            }
            double[] tmGridInterp = new double[interpolatedData.volsForDt.get(indexFut).volsForFut.size()];
            for (int i = 0; i < tmGridInterp.length; i++) {
                tmGridInterp[i] = interpolatedData.volsForDt.get(indexFut).volsForFut.get(i).tm;
            }
            //Check that the number of strikes (across the time to maturities) is the same for the
            //interpolated data. Note that the number of strikes across the different time to
            //maturities may be different in the original data
            int numStrikesInterp = interpolatedData.volsForDt.get(indexFut).volsForFut.get(0).ln_K_over_S.length;
            for (int i = 1; i < interpolatedData.volsForDt.get(indexFut).volsForFut.size(); i++) {
                int numStrikesCurr = interpolatedData.volsForDt.get(indexFut).volsForFut.get(i).ln_K_over_S.length;
                if (numStrikesCurr != numStrikesInterp) {
                    throw new Error("performTmReinterpolated : the number of strikes are not even for the interplated data");
                }
            }
            //We preserve the strikes, and map onto the time to maturities of the original data.
            //Then we reinterpolate onto the original strikes separately
            for (int i = 0; i < tmGridOrig.length; i++) {
                Data.VolsForGivenTm volsForTm = new Data.VolsForGivenTm(numStrikesInterp);
                volsFutInterp.volsForFut.add(volsForTm);
            }
            for (int indexStrike = 0; indexStrike < numStrikesInterp; indexStrike++) {
                double[] y = new double[tmGridInterp.length];
                for (int i = 0; i < tmGridInterp.length; i++) {
                    y[i] = interpolatedData.volsForDt.get(indexFut).volsForFut.get(i).v[indexStrike];
                }
                double[] yGridVals;
                if (useLinearInVolForTm == true) {
                    boolean linearExtrapolation = false;
                    yGridVals = interpLinear.interpLinear(tmGridInterp, y, tmGridOrig, linearExtrapolation);
                } else {
                    yGridVals = interpolateAsVariance(tmGridInterp, y, tmGridOrig);
                }
                for (int i = 0; i < tmGridOrig.length; i++) {
                    volsFutInterp.volsForFut.get(i).tm = tmGridOrig[i];
                    volsFutInterp.volsForFut.get(i).v[indexStrike] = yGridVals[i];
                    volsFutInterp.volsForFut.get(i).ln_K_over_S[indexStrike] = interpolatedData.volsForDt.get(indexFut).volsForFut.get(0).ln_K_over_S[indexStrike];
                }
            }
            volsDtInterp.volsForDt.add(volsFutInterp);
        }
        return volsDtInterp;
    }

    public static void outputStrikeReinterpolated(MutableList<Data.VolsForGivenDt> originalData,
                                                  MutableList<Data.VolsForGivenDt> interpolatedData,
                                                  String outputLoc)
            throws IOException {

        if (originalData.size() != interpolatedData.size()) {
            throw new Error("outputStrikeReinterpolated : mismatch between sizes of data sets");
        }

        FileWriter fw = new FileWriter(outputLoc);
        PrintWriter out = new PrintWriter(fw);

        out.print("Dt,F,Tm,ln(K/S),v(original),v(reinterp),price(original),price(reinterp)\n");

        for (int indexDay = 0; indexDay < originalData.size(); indexDay++) {
            if (interpolatedData.get(indexDay).dt != originalData.get(indexDay).dt) {
                throw new Error("outputStrikeReinterpolated : mismatch between original and interpolated data");
            }
            if (interpolatedData.get(indexDay).volsForDt.size() != originalData.get(indexDay).volsForDt.size()) {
                throw new Error("outputStrikeReinterpolated : mismatch between original and interpolated data");
            }

            for (int indexFut = 0; indexFut < originalData.get(indexDay).volsForDt.size(); indexFut++) {
                Data.VolsForGivenFut volsFut = originalData.get(indexDay).volsForDt.get(indexFut);

                if (interpolatedData.get(indexDay).volsForDt.get(indexFut).F != volsFut.F) {
                    throw new Error("outputStrikeReinterpolated : mismatch between original and interpolated data");
                }
                if (interpolatedData.get(indexDay).volsForDt.get(indexFut).volsForFut.size() != volsFut.volsForFut.size()) {
                    throw new Error("outputStrikeReinterpolated : mismatch between original and interpolated data");
                }

                Data.VolsForGivenFut volsFutInterp = new Data.VolsForGivenFut();
                volsFutInterp.F = volsFut.F;

                for (int indexTm = 0; indexTm < volsFut.volsForFut.size(); indexTm++) {
                    Data.VolsForGivenTm volsTm = volsFut.volsForFut.get(indexTm);

                    if (interpolatedData.get(indexDay).volsForDt.get(indexFut).volsForFut.get(indexTm).tm != volsTm.tm) {
                        throw new Error("outputStrikeReinterpolated : mismatch between original and interpolated data");
                    }

                    if (interpolatedData.get(indexDay).volsForDt.get(indexFut).volsForFut.get(indexTm).ln_K_over_S.length
                            != volsTm.ln_K_over_S.length) {
                        throw new Error("outputStrikeReinterpolated : mismatch between original and interpolated data");
                    }

                    if (interpolatedData.get(indexDay).volsForDt.get(indexFut).volsForFut.get(indexTm).v.length
                            != volsTm.v.length) {
                        throw new Error("outputStrikeReinterpolated : mismatch between original and interpolated data");
                    }

                    for (int i = 0; i < volsTm.ln_K_over_S.length; i++) {
                        double strike = Math.exp(volsTm.ln_K_over_S[i]) * volsFut.F;
                        double originalPrice, interpPrice;
                        //Note that in the input data the prices are all calls
                        if (strike < volsFut.F) {
                            originalPrice = BlackScholesFormula.calculate(false,
                                    volsFut.F,
                                    strike,
                                    0.0,
                                    volsTm.tm,
                                    volsTm.v[i] / 100.);
                            interpPrice = BlackScholesFormula.calculate(false,
                                    volsFut.F,
                                    strike,
                                    0.0,
                                    volsTm.tm,
                                    interpolatedData.get(indexDay).volsForDt.get(indexFut).volsForFut.get(indexTm).v[i] / 100.);
                        } else {
                            originalPrice = BlackScholesFormula.calculate(true,
                                    volsFut.F,
                                    strike,
                                    0.0,
                                    volsTm.tm,
                                    volsTm.v[i] / 100.);
                            interpPrice = BlackScholesFormula.calculate(true,
                                    volsFut.F,
                                    strike,
                                    0.0,
                                    volsTm.tm,
                                    interpolatedData.get(indexDay).volsForDt.get(indexFut).volsForFut.get(indexTm).v[i] / 100.);
                        }
                        out.print(originalData.get(indexDay).dt);
                        out.print(",");
                        out.print(volsFut.F);
                        out.print(",");
                        out.print(volsTm.tm);
                        out.print(",");
                        out.print(volsTm.ln_K_over_S[i]);
                        out.print(",");
                        out.print(volsTm.v[i]);
                        out.print(",");
                        out.print(interpolatedData.get(indexDay).volsForDt.get(indexFut).volsForFut.get(indexTm).v[i]);
                        out.print(",");
                        out.print(originalPrice);
                        out.print(",");
                        out.print(interpPrice);
                        out.print("\n");
                    }
                }
            }
        }
        out.flush();
        out.close();
        fw.close();
    }

    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------

    //checkReinterpolation : when this is true, the vols are reinterpolated onto the grid points of the
    //market data and checks are performed to see discrepancies to the original price
    public static void constructGrid(String fullPath_Input_VolGridTmSpacings,
                                     String fullPath_Input_VolGridMoneynessSpacings,
                                     String fullPath_Input_VolMarketData,
                                     String fullPath_Output_VolGrid,
                                     Boolean checkInputVolsAgainstInputPrices,
                                     Boolean checkReinterpolation,
                                     String fullPath_Output_FullReinterpolation,
                                     String fullPath_Output_StrikeOnlyReinterpolation,
                                     Boolean outputGrid,
                                     String fullPath_Output_ATMVols)
            throws IOException {
        // Call the parse method and put the results in a table.
        TableModel tParamTm = CSVParser.parse(new File(fullPath_Input_VolGridTmSpacings));
        TableModel tParamLnStrike = CSVParser.parse(new File(fullPath_Input_VolGridMoneynessSpacings));
        TableModel t = CSVParser.parse(new File(fullPath_Input_VolMarketData));

        boolean useLinearInVol = false;
        boolean inTermsOfPrice = true;

        if (checkInputVolsAgainstInputPrices == true) {
            Checking.checkVolsReconcileWithPrices(t, inTermsOfPrice);
        }

        //convert tm from days to yearfracs
        for (int i = 0; i < tParamTm.getRowCount(); ++i) {
            Double tm = (Double.parseDouble(tParamTm.getValueAt(i, 0).toString()) / 365);
            tParamTm.setValueAt(tm, i, 0);
        }

        Checking.ReinterpolationMetrics check = new Checking.ReinterpolationMetrics();
        check.performReinterpolation = true;
        Checking.ReinterpolationMetrics dontCheck = new Checking.ReinterpolationMetrics();
        dontCheck.performReinterpolation = false;
        MutableList<Data.VolsForGivenDt> extractedVols = DataExtraction.extractVols(t);
        MutableList<Data.VolsForGivenDt> reinterpVols = FastList.newList();
        MutableList<Data.VolsForGivenDt> reinterpVolsOnStrikeOnly = FastList.newList();
        double[] paramTmArr = DataExtraction.convertParametersToArray(tParamTm);
        double[] paramLnArrStrike = DataExtraction.convertParametersToArray(tParamLnStrike);
        BufferedOutput outputWriter = null;
        BufferedOutput outputWriterATM;
        double[] atm = new double[1];
        atm[0] = 0.;

        if (outputGrid == true) {
            outputWriter = new BufferedOutput(fullPath_Output_VolGrid);
        }

        outputWriterATM = new BufferedOutput(fullPath_Output_ATMVols);

        for (int indexDt = 0; indexDt < extractedVols.size(); indexDt++) {
            Data.VolsForGivenDt marketInput = extractedVols.get(indexDt);
            Data.VolsForGivenDt interpDataAcrossStrike = Interpolation.performStrikeInterpolation(marketInput, paramLnArrStrike, check, useLinearInVol);
            if (checkReinterpolation == true) {
                Data.VolsForGivenDt reinterpolatedDataAcrossStrike = Interpolation.performStrikeReinterpolated(marketInput, interpDataAcrossStrike, useLinearInVol);
                reinterpVolsOnStrikeOnly.add(reinterpolatedDataAcrossStrike);
            }
            Data.VolsForGivenDt interpDataTime = Interpolation.performTmInterpolation(interpDataAcrossStrike, paramTmArr, useLinearInVol);
            Data.VolsForGivenDt atmVols = Interpolation.performStrikeInterpolation(interpDataTime, atm, dontCheck, useLinearInVol);
            outputWriterATM.writeATMS(atmVols);
            if (checkReinterpolation == true) {
                Data.VolsForGivenDt reinterpolatedDataAcrossTime = Interpolation.performTmReinterpolated(marketInput, interpDataTime, useLinearInVol);
                Data.VolsForGivenDt reinterpolatedDataAcrossTimeThenStrike = Interpolation.performStrikeReinterpolated(marketInput, reinterpolatedDataAcrossTime, useLinearInVol);
                reinterpVols.add(reinterpolatedDataAcrossTimeThenStrike);
            }
            if (outputGrid == true) {
                outputWriter.write(interpDataTime);
            }
        }
        if (checkReinterpolation == true) {
            Interpolation.outputStrikeReinterpolated(extractedVols, reinterpVols, fullPath_Output_FullReinterpolation);
            Interpolation.outputStrikeReinterpolated(extractedVols, reinterpVolsOnStrikeOnly, fullPath_Output_StrikeOnlyReinterpolation);
        }
        check.printMetrics();
        if (outputGrid == true) {
            outputWriter.Finish();
        }
        outputWriterATM.Finish();
    }

    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------

}
