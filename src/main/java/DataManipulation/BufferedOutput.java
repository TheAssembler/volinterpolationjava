package DataManipulation;

import com.gs.collections.api.list.MutableList;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

//Because the grids were taking up so much memory the computer (because
//I was storing the whole interpolation grid in memory) that on 32 bit
//machine I was getting out of memory error message. This attempts
//to output the interpolated grid in stages, avoiding the need to
//store everything in memory
public class BufferedOutput {
    FileWriter fw;
    PrintWriter out;
    boolean hasNumStrikesBeenSet;
    double[] ln_K_over_S;

    public BufferedOutput(String output) throws IOException {
        fw = new FileWriter(output);
        out = new PrintWriter(fw);
        hasNumStrikesBeenSet = false;
    }

    public void write(Data.VolsForGivenDt arg) {
        if (hasNumStrikesBeenSet == false) {
            out.print("Dt,F,Tm");
            for (int indexFut = 0; indexFut < 1; indexFut++) {
                for (int indexTm = 0; indexTm < 1; indexTm++) {
                    ln_K_over_S = arg.volsForDt.get(indexFut).volsForFut.get(indexTm).ln_K_over_S;
                    for (int indexStrike = 0; indexStrike < ln_K_over_S.length; indexStrike++) {
                        out.print(",");
                        out.print(ln_K_over_S[indexStrike]);
                    }
                    out.print("\n");
                }
            }
            hasNumStrikesBeenSet = true;
        }

        for (int indexFut = 0; indexFut < arg.volsForDt.size(); indexFut++) {
            for (int indexTm = 0; indexTm < arg.volsForDt.get(indexFut).volsForFut.size(); indexTm++) {
                out.print(arg.dt);
                out.print(",");
                out.print(arg.volsForDt.get(indexFut).F);
                out.print(",");
                out.print(arg.volsForDt.get(indexFut).volsForFut.get(indexTm).tm);
                double[] Tm_ln_K_over_S = arg.volsForDt.get(indexFut).volsForFut.get(indexTm).ln_K_over_S;
                double[] v = arg.volsForDt.get(indexFut).volsForFut.get(indexTm).v;
                if (Tm_ln_K_over_S.length != ln_K_over_S.length) {
                    throw new Error("BufferedOutput : size of strike grid has changed");
                }
                for (int indexStrike = 0; indexStrike < ln_K_over_S.length; indexStrike++) {
                    if (Tm_ln_K_over_S[indexStrike] != ln_K_over_S[indexStrike]) {
                        throw new Error("BufferedOutput : strike mismatch detected");
                    }
                    out.print(",");
                    out.print(v[indexStrike]);
                }
                out.print("\n");
            }
        }
    }

    public void writeATMS(Data.VolsForGivenDt arg) {
        if (hasNumStrikesBeenSet == false) {
            out.print("Dt,Index,F");
            for (int indexFut = 0; indexFut < 1; indexFut++) {
                MutableList<Data.VolsForGivenTm> volsForFut = arg.volsForDt.get(indexFut).volsForFut;
                for (int indexTm = 0; indexTm < volsForFut.size(); indexTm++) {
                    out.print(",");
                    out.print(volsForFut.get(indexTm).tm);
                    if (volsForFut.get(indexTm).ln_K_over_S.length != 1) {
                        throw new Error("writeATMS : more than one strike when we should only have ATM");
                    }
                }
                out.print("\n");
            }
            hasNumStrikesBeenSet = true;
        }
        for (int indexFut = 0; indexFut < arg.volsForDt.size(); indexFut++) {
            out.print(arg.dt);
            out.print(",");
            out.print(indexFut + 1);
            out.print(",");
            out.print(arg.volsForDt.get(indexFut).F);
            MutableList<Data.VolsForGivenTm> volsForFut = arg.volsForDt.get(indexFut).volsForFut;
            for (int indexTm = 0; indexTm < volsForFut.size(); indexTm++) {
                out.print(",");
                out.print(volsForFut.get(indexTm).v[0]);
                if (volsForFut.get(indexTm).v.length != 1) {
                    throw new Error("writeATMS : more than one strike when we should only have ATM");
                }
            }
            out.print("\n");
        }
    }

    public void Finish() throws IOException {
        out.flush();
        out.close();
        fw.close();
    }
}
