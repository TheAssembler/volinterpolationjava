package DataManipulation;

import java.util.Comparator;

import com.gs.collections.api.list.MutableList;
import com.gs.collections.impl.list.mutable.FastList;

public class Data {

    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------

    //This is the datastructure used to store the input data. Note that the data
    //is transformed into the structures volsForGivenTm prior to interpolation
    public static class Underlying {
        //Note that dt and F have been made final, because they are labels
        //that should not change even though the interpolation
        public final String dt;
        public double tm;
        public final double F;
        public double v;
        public double ln_K_over_S;

        public Underlying(String dt, double tm, double F, double v, double ln_K_over_S) {
            this.dt = dt;
            this.tm = tm;
            this.F = F;
            this.v = v;
            this.ln_K_over_S = ln_K_over_S;
        }
    }
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------

    //The data structures in this sector are used to store the input data
    //prior to interpolation
    public static class VolsForGivenTm {
        public double tm;
        public double[] v;
        public double[] ln_K_over_S;

        public VolsForGivenTm(int size) {
            v = new double[size];
            ln_K_over_S = new double[size];
        }
    }

    public static class VolsForGivenFut {
        public double F;
        public MutableList<VolsForGivenTm> volsForFut;

        VolsForGivenFut() {
            volsForFut = FastList.newList();
        }
    }

    public static class VolsForGivenDt {
        public String dt;
        public MutableList<VolsForGivenFut> volsForDt;

        VolsForGivenDt() {
            volsForDt = FastList.newList();
        }
    }

    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------

    public static class VolsForGivenTmComparator implements Comparator<VolsForGivenTm> {
        public int compare(VolsForGivenTm left, VolsForGivenTm right) {
            return (left.tm < right.tm ? -1 : left.tm == right.tm ? 0 : 1);
        }
    }

    //Note that we do not sort by the actual future value, but instead sort by the first
    //time to maturity associated with each future. Therefore we have the futures ordered
    //in terms of front month etc. Theoretically sorting by future value should achieve
    //the same thing, but I didnt want to assume this holds.
    //Note the core assumption is that the time to maturities have already been sorted.
    public static class VolsForGivenFutComparator implements Comparator<VolsForGivenFut> {
        public int compare(VolsForGivenFut left, VolsForGivenFut right) {
            return (left.volsForFut.get(0).tm < right.volsForFut.get(0).tm ? -1 : left.volsForFut.get(0).tm == right.volsForFut.get(0).tm ? 0 : 1);
        }
    }

    public static class VolsForGivenDtComparator implements Comparator<VolsForGivenDt> {
        public int compare(VolsForGivenDt left, VolsForGivenDt right) {
            return left.dt.compareTo(right.dt);
        }
    }

    public static class UnderlyingComparator implements Comparator<Underlying> {
        public int compare(Underlying left, Underlying right) {
            return (left.ln_K_over_S < right.ln_K_over_S ? -1 : left.ln_K_over_S == right.ln_K_over_S ? 0 : 1);
        }
    }

    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------

}
