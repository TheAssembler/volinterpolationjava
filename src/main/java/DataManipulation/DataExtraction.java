package DataManipulation;

import com.gs.collections.api.RichIterable;
import com.gs.collections.api.list.MutableList;
import com.gs.collections.api.multimap.list.MutableListMultimap;
import com.gs.collections.impl.list.mutable.FastList;
import com.gs.collections.impl.multimap.list.FastListMultimap;

import javax.swing.table.TableModel;

public class DataExtraction {

    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------

    //This function groups the input data by market date. The Key is the string
    //corresponding to the date.
    public static MutableListMultimap<String, Data.Underlying> aggregate_By_Dt(TableModel t) {

        final MutableListMultimap<String, Data.Underlying> inputDataGroupedByDate = FastListMultimap.newMultimap();

        int i = 0;
        int cDt = 0;
        int cTm = 1;
        int cF = 2;
        int cK = 3;
        int cV = 4;
        int dataRows = t.getRowCount();
        String dt;
        Double tm, F, K, v, lnK_Over_S;

        while (i < dataRows) {
            dt = t.getValueAt(i, cDt).toString();
            tm = Double.parseDouble(t.getValueAt(i, cTm).toString());
            F = Double.parseDouble(t.getValueAt(i, cF).toString());
            K = Double.parseDouble(t.getValueAt(i, cK).toString());
            v = Double.parseDouble(t.getValueAt(i, cV).toString());
            lnK_Over_S = Math.log(K / F);
            inputDataGroupedByDate.put(dt, new Data.Underlying(dt, tm, F, v, lnK_Over_S));
            i++;
        }

        return inputDataGroupedByDate;
    }

    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------

    //This function groups the input data by Future. The Key is the double
    //corresponding to the future value.
    public static MutableListMultimap<Double, Data.Underlying> aggregate_By_Fut(MutableList<Data.Underlying> t) {

        final MutableListMultimap<Double, Data.Underlying> inputDataGroupedByFut = FastListMultimap.newMultimap();

        int i = 0;
        int dataRows = t.size();
        Double F;

        while (i < dataRows) {
            //F = t.get(i).F;
            F = 1.;
            inputDataGroupedByFut.put(F, t.get(i));
            i++;
        }

        return inputDataGroupedByFut;
    }

    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------

    //This function groups the input data by Time to mat. The Key is the double
    //corresponding to the time to maturity value.
    public static MutableListMultimap<Double, Data.Underlying> aggregate_By_Tm(MutableList<Data.Underlying> t) {

        final MutableListMultimap<Double, Data.Underlying> inputDataGroupedByTm = FastListMultimap.newMultimap();

        int i = 0;
        int dataRows = t.size();
        Double tm;

        while (i < dataRows) {
            tm = t.get(i).tm;
            inputDataGroupedByTm.put(tm, t.get(i));
            i++;
        }

        return inputDataGroupedByTm;
    }

    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------

    public static MutableList<Double> convertParametersToList(TableModel t) {
        int dataRows = t.getRowCount();
        int i = 0;
        MutableList<Double> parameters = FastList.newList();

        while (i < dataRows) {
            if (t.getValueAt(i, 0) != null) {
                Double val = Double.parseDouble(t.getValueAt(i, 0).toString());
                parameters.add(val);
            }
            i++;
        }

        parameters.sortThis();
        return parameters;
    }

    public static double[] convertParametersToArray(TableModel t) {
        MutableList<Double> parameters = convertParametersToList(t);
        Double[] result = new Double[1];
        result = parameters.toArray(result);
        double[] ret = Maths.Utils.convert_Arr_Double_to_double(result);
        return ret;
    }

    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------

    public static MutableList<Data.VolsForGivenDt> extractVols(TableModel t) {

        MutableListMultimap<String, Data.Underlying> by_Dt = DataExtraction.aggregate_By_Dt(t);

        MutableList<Data.VolsForGivenDt> allVols = FastList.newList();

        //***** Iterate over the dates *****
        RichIterable<String> inputDtKeysI = by_Dt.keysView();
        Object[] inputDtKeys = inputDtKeysI.toArray();

        for (int i = 0; i < inputDtKeys.length; i++) {

            Data.VolsForGivenDt volsDt = new Data.VolsForGivenDt();

            String dt = (String) inputDtKeys[i];
            volsDt.dt = dt;
            MutableList<Data.Underlying> dataForDt = by_Dt.get(dt);
            MutableListMultimap<Double, Data.Underlying> dataByFut = DataExtraction.aggregate_By_Fut(dataForDt);

            //***** Iterate over the futures *****
            RichIterable<Double> inputFutKeysI = dataByFut.keysView();
            Object[] inputFutKeys = inputFutKeysI.toArray();

            for (int j = 0; j < inputFutKeys.length; j++) {

                Data.VolsForGivenFut volsFut = new Data.VolsForGivenFut();

                Double fut = (Double) inputFutKeys[j];
                volsFut.F = fut;
                MutableList<Data.Underlying> dataForFut = dataByFut.get(fut);
                MutableListMultimap<Double, Data.Underlying> dataByTm = DataExtraction.aggregate_By_Tm(dataForFut);

                //***** Iterate over the time to maturity *****
                RichIterable<Double> inputTmKeysI = dataByTm.keysView();
                Object[] inputTmKeys = inputTmKeysI.toArray();

                for (int k = 0; k < inputTmKeys.length; k++) {
                    Double tm = (Double) inputTmKeys[k];

                    MutableList<Data.Underlying> dataForTm = sortImmutableList(dataByTm.get(tm));

                    Data.VolsForGivenTm volsTm = new Data.VolsForGivenTm(dataForTm.size());

                    volsTm.tm = tm;
                    for (int l = 0; l < dataForTm.size(); l++) {
                        volsTm.ln_K_over_S[l] = dataForTm.get(l).ln_K_over_S;
                        volsTm.v[l] = dataForTm.get(l).v;
                    }

                    boolean atmFound = false;
                    for (int l = 0; l < volsTm.ln_K_over_S.length; l++) {
                        if (volsTm.ln_K_over_S[l] == 0.0) {
                            atmFound = true;
                            break;
                        }
                    }
                    if (atmFound == false) {
                        throw new Error("extractVols : atm vols not found");
                    }
                    volsFut.volsForFut.add(volsTm);
                }
                volsFut.volsForFut.sortThis(new Data.VolsForGivenTmComparator());
                volsDt.volsForDt.add(volsFut);
            }
            //Note that sorting by VolsForGivenFutComparator now assumes that
            //VolsForGivenTmComparator has already been performed
            volsDt.volsForDt.sortThis(new Data.VolsForGivenFutComparator());
            allVols.add(volsDt);
        }
        allVols.sortThis(new Data.VolsForGivenDtComparator());
        return allVols;
    }

    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------

    public static MutableList<Data.Underlying> sortImmutableList(MutableList<Data.Underlying> arg) {
        MutableList<Data.Underlying> result = FastList.newList();
        for (int l = 0; l < arg.size(); l++) {
            result.add(arg.get(l));
        }
        result.sortThis(new Data.UnderlyingComparator());
        //Remove duplicates
        if (arg.size() > 1) {
            for (int l = arg.size() - 1; l > 0; l--) {
                if (result.get(l).ln_K_over_S == result.get(l - 1).ln_K_over_S) {
                    result.remove(l);
                }
            }
        }
        return result;
    }

    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------

}
