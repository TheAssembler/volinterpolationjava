import DataManipulation.Interpolation;

import java.io.*;

public class CreateFixedGrid {

    private static String paramTm = "input_volGrid_TM.txt";
    private static String paramLn = "input_volGrid_Moneyness.txt";
    private static String outputReinterp = "results_reinterp";
    private static String outputReinterpStrikeOnly = "results_reinterp_StrikeOnly";
    private static String outputATMs = "results_ATM";
    private static String output = "results_Grid";
    private static Boolean checkInputVolsAgainstInputPrices = false;
    private static Boolean checkReinterpolation = true;
    private static Boolean outputGrid =true;

    public static void runSim(int iIden, int iDir,
                              String[] iden, String[] dirs, String[] marketFile)
            throws IOException {
        Interpolation.constructGrid(dirs[iDir] + paramTm, dirs[iDir] + paramLn,
                dirs[iDir] + marketFile[iIden],
                dirs[iDir] + output + "_" + iden[iIden] + ".csv",
                checkInputVolsAgainstInputPrices, checkReinterpolation,
                dirs[iDir] + outputReinterp + "_" + iden[iIden] + ".csv",
                dirs[iDir] + outputReinterpStrikeOnly + "_" + iden[iIden] + ".csv",
                outputGrid, dirs[iDir] + outputATMs + "_" + iden[iIden] + ".csv");
    }

    public static void main(String[] args) throws IOException {
        /*
            Bund = OGBL
            Bobl = OGBM
            Schatz = OGBS
        */

        String mkData, dir;

        dir = "Test/";
        mkData = "MarketData.csv";

        String[] dirs = new String[7];
        String[] marketFile = new String[13];
        String[] iden = new String[13];

        iden[0] = "EuroI";
        iden[1] = "EuroL";
        iden[2] = "Euronext_K";
        iden[3] = "Euronext_K2";
        iden[4] = "Euronext_K3";
        iden[5] = "Euronext_K4";
        iden[6] = "Euronext_M";
        iden[7] = "Euronext_M2";
        iden[8] = "Euronext_M3";
        iden[9] = "Euronext_M4";
        iden[10] = "OGBL";
        iden[11] = "OGBS";
        iden[12] = "OGBM";

        dirs[0] = "marketData_vol/EuroI/";
        dirs[1] = "NewData/EuroL/";
        dirs[2] = "NewData/Euronext_K/";
        dirs[3] = "NewData/Euronext_M/";
        dirs[4] = "NewData/OGBL/";
        dirs[5] = "NewData/OGBS/";
        dirs[6] = "marketData_vol/OGBM/";

        marketFile[0] = "input_Euronext_I_full.csv";
        marketFile[1] = "input_Euronext_L_full.csv";
        marketFile[2] = "input_Euronext_K_full.csv";
        marketFile[3] = "input_Euronext_K2_full.csv";
        marketFile[4] = "input_Euronext_K3_full.csv";
        marketFile[5] = "input_Euronext_K4_full.csv";
        marketFile[6] = "input_Euronext_M_full.csv";
        marketFile[7] = "input_Euronext_M2_full.csv";
        marketFile[8] = "input_Euronext_M3_full.csv";
        marketFile[9] = "input_Euronext_M4_full.csv";
        marketFile[10] = "input_OGBL_full.csv";
        marketFile[11] = "input_OGBS_full.csv";
        marketFile[12] = "input_OGBM_full.csv";

        int iIden;
        int iDir;
        int iMarketFile;

        //iIden = 0; iDir = 0;
        //runSim(iIden, iDir, iden, dirs, marketFile);

        //iIden = 1; iDir = 1;
        //runSim(iIden, iDir, iden, dirs, marketFile);

        //iIden = 2; iDir = 2;
        //runSim(iIden, iDir, iden, dirs, marketFile);

        //iIden = 3; iDir = 2;
        //runSim(iIden, iDir, iden, dirs, marketFile);

        //iIden = 4; iDir = 2;
        //runSim(iIden, iDir, iden, dirs, marketFile);

        //iIden = 5; iDir = 2;
        //runSim(iIden, iDir, iden, dirs, marketFile);

        //iIden = 6; iDir = 3;
        //runSim(iIden, iDir, iden, dirs, marketFile);

        //iIden = 7; iDir = 3;
        //runSim(iIden, iDir, iden, dirs, marketFile);

        //iIden = 8; iDir = 3;
        //runSim(iIden, iDir, iden, dirs, marketFile);

        //iIden = 9; iDir = 3;
        //runSim(iIden, iDir, iden, dirs, marketFile);

        //iIden = 10; iDir = 4;
        //runSim(iIden, iDir, iden, dirs, marketFile);

        //iIden = 11; iDir = 5;
        //runSim(iIden, iDir, iden, dirs, marketFile);

        iIden = 12; iDir = 6;
        runSim(iIden, iDir, iden, dirs, marketFile);

        System.out.println("finished");
    }

}
