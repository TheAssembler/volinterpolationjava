package Parsing;

import javax.swing.table.TableModel;
import java.io.File;
import java.io.IOException;

public class CompareFiles {
    public static boolean areFilesIdentital(String fileToCompare, String originalFile) throws IOException {
        TableModel tFileToCompare = CSVParser.parse(new File(fileToCompare));
        TableModel tOriginalFile = CSVParser.parse(new File(originalFile));

        boolean areSame = true;
        String cellOriginal, cellToCompare;

        if (tOriginalFile.getRowCount() != tFileToCompare.getRowCount()) {
            throw new Error("areFilesIdentital : rows of the two files are not identical");
        }

        if (tOriginalFile.getColumnCount() != tFileToCompare.getColumnCount()) {
            throw new Error("areFilesIdentital : columns of the two files are not identical");
        }

        for (int row = 0; row < tOriginalFile.getRowCount(); row++) {
            for (int col = 0; col < tOriginalFile.getColumnCount(); col++) {
                cellOriginal = tOriginalFile.getValueAt(row, col).toString();
                cellToCompare = tFileToCompare.getValueAt(row, col).toString();
                if (!cellOriginal.equals(cellToCompare)) {
                    areSame = false;
                    break;
                }
            }
        }

        return areSame;
    }
}
