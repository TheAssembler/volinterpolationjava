package Parsing;//http://www.ehow.com/how_6796642_read-csv-file-java.html

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class CSVParser {
    //note that there can not be any empty lines. There can be a carraige return after the
    //last entry.
    public static TableModel parse(File f) throws FileNotFoundException {
        int line = 0;
        ArrayList<String> headers = new ArrayList<String>();

        // Get the headers of the table.
        Scanner lineScan = new Scanner(f);
        Scanner s = new Scanner(lineScan.nextLine());
        s.useDelimiter(",");
        while (s.hasNext()) {
            headers.add(s.next());
        }

        int cols = headers.size();
        int rows = 0;

        while (lineScan.hasNextLine()) {
            s = new Scanner(lineScan.nextLine());
            rows++;
        }

        String[][] data = new String[rows][cols];

        lineScan = new Scanner(f);
        String header = lineScan.nextLine();
        int c;

        // Go through each line of the table and add each cell to the ArrayList
        while (lineScan.hasNextLine()) {
            c = 0;
            s = new Scanner(lineScan.nextLine());
            s.useDelimiter(", *");
            while (s.hasNext()) {
                data[line][c] = s.next();
                c++;
            }
            line++;
        }

        // Create a table and return it.
        return new DefaultTableModel(data, headers.toArray());
    }
}
